package org.example;

import java.util.*;

public class Main {

    public static final String PRENOM_CAMPING = "Michel";
    public static final int AGE_CAMPING = 50;

    public static String affichageSimple(Personne personne) {
        return personne.toString();
    }

    public static String affichageMultiple(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        affichage.append(personnes.get(0).toString());
        for (int i = 1; i < personnes.size(); i++) {
            affichage.append(" et ");
            affichage.append(personnes.get(i).toString());
        }
        return affichage.toString();
    }

    public static String adoreLeCamping(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        for (Personne personne : personnes) {
            if (personne.getPrenom().equals(PRENOM_CAMPING) || personne.getAge() >= AGE_CAMPING) {
                affichage.append("On a dans la liste quelqu'un qui adore le camping !\n");
                break;
            }
        }
        return affichage.toString();
    }

    public static String quiAdoreLeCamping(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        for (Personne personne : personnes) {
            if (personne.getPrenom().equals(PRENOM_CAMPING) || personne.getAge() >= AGE_CAMPING) {
                affichage.append("C'est formidable ");
                affichage.append(affichageSimple(personne));
                affichage.append(" adore le camping !\n");
            }
        }
        return affichage.toString();
    }

    public static String tailleIdentiqueNomPrenom(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        for (Personne personne : personnes) {
            if (personne.getPrenom().length() == personne.getNom().length()) {
                affichage.append("C'est formidable ");
                affichage.append(affichageSimple(personne));
                affichage.append(" a ");
                affichage.append(personne.getPrenom().length());
                affichage.append(" lettres dans son nom et son prénom !\n");
            }
        }
        return affichage.toString();
    }

    public static String prenomsIdentiques(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        List<String> listePrenoms = new ArrayList<>();
        Map<String, Integer> nombrePrenoms = new HashMap<>();
        for (Personne personne : personnes) {
            if (nombrePrenoms.get(personne.getPrenom()) == null) {
                listePrenoms.add(personne.getPrenom());
                nombrePrenoms.put(personne.getPrenom(), 1);
            } else {
                nombrePrenoms.put(personne.getPrenom(), nombrePrenoms.get(personne.getPrenom()) + 1);
            }
        }
        for (String prenom : listePrenoms) {
            List<Personne> filtrePersonnes = new ArrayList<>();
            for (Personne personne : personnes) {
                if (nombrePrenoms.get(prenom) > 1 && personne.getPrenom().equals(prenom)) {
                    filtrePersonnes.add(personne);
                }
            }
            if (!filtrePersonnes.isEmpty()) {
                affichage.append(affichageMultiple(filtrePersonnes));
                affichage.append(" ont des prénoms identiques.\n");
            }
        }
        return affichage.toString();
    }

    public static String nomsIdentiques(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        List<String> listeNoms = new ArrayList<>();
        Map<String, Integer> nombreNoms = new HashMap<>();
        for (Personne personne : personnes) {
            if (nombreNoms.get(personne.getNom()) == null) {
                listeNoms.add(personne.getNom());
                nombreNoms.put(personne.getNom(), 1);
            } else {
                nombreNoms.put(personne.getNom(), nombreNoms.get(personne.getNom()) + 1);
            }
        }
        for (String nom : listeNoms) {
            List<Personne> filtrePersonnes = new ArrayList<>();
            for (Personne personne : personnes) {
                if (nombreNoms.get(nom) > 1 && personne.getNom().equals(nom)) {
                    filtrePersonnes.add(personne);
                }
            }
            if (!filtrePersonnes.isEmpty()) {
                affichage.append(affichageMultiple(filtrePersonnes));
                affichage.append(" ont des noms identiques.\n");
            }
        }
        return affichage.toString();
    }

    public static String agesIdentiques(List<Personne> personnes) {
        StringBuilder affichage = new StringBuilder();
        List<Integer> listeAges = new ArrayList<>();
        Map<Integer, Integer> nombreAges = new HashMap<>();
        for (Personne personne : personnes) {
            if (nombreAges.get(personne.getAge()) == null) {
                listeAges.add(personne.getAge());
                nombreAges.put(personne.getAge(), 1);
            } else {
                nombreAges.put(personne.getAge(), nombreAges.get(personne.getAge()) + 1);
            }
        }
        for (int age : listeAges) {
            List<Personne> filtrePersonnes = new ArrayList<>();
            for (Personne personne : personnes) {
                if (nombreAges.get(age) > 1 && personne.getAge() == age) {
                    filtrePersonnes.add(personne);
                }
            }
            if (!filtrePersonnes.isEmpty()) {
                affichage.append(affichageMultiple(filtrePersonnes));
                affichage.append(" ont des âges identiques.\n");
            }
        }
        return affichage.toString();
    }

    public static void main(String[] args) {
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne("Pascal", "ROSE", 43));
        personnes.add(new Personne("Mickaël", "FLEUR", 29));
        personnes.add(new Personne("Henri", "TULIPE", 35));
        personnes.add(new Personne("Michel", "FRAMBOISE", 35));
        personnes.add(new Personne("Arthur", "PETALE", 35));
        personnes.add(new Personne("Michel", "POLLEN", 50));
        personnes.add(new Personne("Michel", "FRAMBOISE", 42));
        Collections.shuffle(personnes);
        System.out.print(adoreLeCamping(personnes));
        System.out.print(quiAdoreLeCamping(personnes));
        System.out.print(tailleIdentiqueNomPrenom(personnes));
        System.out.print(prenomsIdentiques(personnes));
        System.out.print(nomsIdentiques(personnes));
        System.out.print(agesIdentiques(personnes));
    }
}