package org.example;

public class Personne {

    private String prenom;
    private String nom;
    private Integer age;

    public Personne() {
        this("Prénom", "NOM", 0);
    }
    public Personne(String prenom, String nom, Integer age) {
        this.setPrenom(prenom);
        this.setNom(nom);
        this.setAge(age);
    }

    public String getPrenom() {
        return this.prenom;
    }
    public String getNom() {
        return this.nom;
    }
    public Integer getAge() {
        return this.age;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setAge(Integer age) {
        if (age >= 0) {
            this.age = age;
        }
    }

    public String toString() {
        return "(" + this.getPrenom() + ", " + this.getNom() + ", " + this.getAge() + ")";
    }

    public static void main(String[] args) {
        Personne personne = new Personne();
        System.out.println(personne);
    }
}